#ifndef _SENSOR_DESCRIPTORS_H
#define _SENSOR_DESCRIPTORS_H

void outputsensorDescriptors(void);

#define HAS_SENSOR_DESCRIPTORS

// $[sensor descriptors]
#define SENSOR_DESCRIPTOR_LIST \
  "CS0.0", \
  "CS0.1", \
  "CS0.2", \
  "CS0.3", \
  "CS0.6", \
  "CS0.7", \




//  Skip center button since it is grounded by default
//  "CS1.4",
// [sensor descriptors]$

#endif


