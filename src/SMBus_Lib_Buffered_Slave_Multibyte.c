//-----------------------------------------------------------------------------
// SMBus_Lib_Buffered_Slave_Multibyte.c
//-----------------------------------------------------------------------------
// Copyright 2014 Silicon Laboratories, Inc.
// http://developer.silabs.com/legal/version/v11/Silicon_Labs_Software_License_Agreement.txt
//
// Program Description:
//
// Example software to demonstrate the SMBus interface in Slave mode using
// the EFM8 SMBus peripheral driver in buffered mode.
// - Interrupt-driven SMBus implementation
// - Only slave states defined
// - Multi-byte SMBus data holders used for both transmit and receive
// - Timer1 used as SMBus clock rate (used only for free timeout detection)
// - Timer3 used by SMBus for SCL low timeout detection
// - SMB0CN0_ARBLOST support included
// - supports multiple-byte writes and multiple-byte reads
// - Pinout:
//    P1.2 -> SDA (SMBus)
//    P1.3 -> SCL (SMBus)
//
//    P1.1 -> LED
//
//    all other port pins unused
//
// How To Test:
//
// 1) Place the switch in "AEM" mode.
// 2) Connect the device to another EFM8 device running SMBus - Master Multibyte code.
// 3) Connect the EFM8SB1 STK board to a PC using a mini USB cable.
// 4) Compile and download code to the EFM8SB1 STK board.
//    In Simplicity Studio IDE, select Run -> Debug from the menu bar,
//    click the Debug button in the quick menu, or press F11.
// 5) Run the code.
//    In Simplicity Studio IDE, select Run -> Resume from the menu bar,
// 6) The slave code will copy the write data to the read
//    data, so a successive write and read will effectively echo the data
//    written.  To verify that the code is working properly, verify on the
//    master that the data received is the same as the data written.
//
// Target:         EFM8SB1
// Tool chain:     Simplicity Studio / Keil C51 9.51
// Command Line:   None
//
// Release 1.0 (BL)
//    - Initial Release
//    - 9 JAN 2015
//
//

//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
//#include "bsp.h"
//#include "uart_0.h"
#include "smb_0.h"
#include "stdio.h"
#include "InitDevice.h"
//#include "string.h"
#include "SMBus_Lib_Buffered_Slave_Multibyte.h"
#include "cslib_config.h"
#include "cslib.h"
#include "profiler_interface.h"
#include "comm_routines.h"
//#include "stdint.h"

//#define NO_OF_CAP_CHANNELS 8
//uint8_t cap_channel_touched[NO_OF_CAP_CHANNELS] =
//  { 0 };

uint8_t cap_channel_2bytes[2];
uint8_t Touch_status = 0;
uint8_t ready_for_new_touch;
SI_SBIT(LED0, SFR_P1, 6);
SI_SBIT(PIN6, SFR_P0, 4);
SI_SBIT(PIN5, SFR_P0, 5);
SI_SBIT(PIN4, SFR_P1, 0);
SI_SBIT(PIN3, SFR_P1, 1);
SI_SBIT(PIN2, SFR_P1, 2);
SI_SBIT(PIN1, SFR_P1, 3);

//-----------------------------------------------------------------------------
// Global VARIABLES
//-----------------------------------------------------------------------------
#define BUFFER_LENGTH 5
SI_SEGMENT_VARIABLE(buffer[BUFFER_LENGTH], uint8_t, SI_SEG_XDATA);

// Global holder for SMBus data
// All receive data is written here
SI_SEGMENT_VARIABLE(SMB_DATA_IN[NUM_BYTES_RD], uint8_t,
                    EFM8PDL_SMB0_RX_BUFTYPE);

// Global holder for SMBus data.
// All transmit data is read from here
SI_SEGMENT_VARIABLE(SMB_DATA_OUT[NUM_BYTES_WR], uint8_t,
                    EFM8PDL_SMB0_TX_BUFTYPE);
volatile bool transferInProgress = false;
// Counter for the number of errors.
uint16_t numErrors;

SI_LOCATED_VARIABLE_NO_INIT(reserved, uint8_t, SI_SEG_XDATA, 0x0000);
//char Tx_ready = 1;
char smb_rec = 0;
//SI_SBIT(SDA, SFR_P1, 2);
//SI_SBIT(SCL, SFR_P1, 3);

//-----------------------------------------------------------------------------
// Pin Definitions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SiLabs_Startup() Routine
// ----------------------------------------------------------------------------
// This function is called immediately after reset, before the initialization
// code is run in SILABS_STARTUP.A51 (which runs before main() ). This is a
// useful place to disable the watchdog timer, which is enable by default
// and may trigger before main() in some instances.
//-----------------------------------------------------------------------------
void
SiLabs_Startup (void)
{
  // Disable the watchdog here
}
//void
//send_string_serial (char *s)
//{
//  char *stmp = s;
//  char c;
//  while ((c = *stmp++) != 0)
//    {
//      while (Tx_ready == 0)
//        ;
//      SBUF0 = c;
//      Tx_ready = 0;
//
//    }
//}
//
//void
//send_data_serial (uint8_t *dataptr, uint8_t length)
//{
//  // uint8_t *stmp = dataptr;
//  uint8_t i = 0;
//  while (i < length)
//    {
//      while (Tx_ready == 0)
//        ;
//
//      SBUF0 = *(dataptr + i);
//      i++;
//      Tx_ready = 0;
//
//    }
//}
void
send_touch_event (uint8_t channel)
{
  SMB_DATA_OUT[0] = channel;

  transferInProgress = true;

  // Start write/read transfer
  // Send out data
  // Receive in data (loopback data from slave)
  SMB0_transfer (SLAVE_ADDR, SMB_DATA_OUT, SMB_DATA_IN, NUM_BYTES_WR,
  NUM_BYTES_RD);

  // Wait until transfer complete callback is called
  while (transferInProgress)
    ;

}
//-----------------------------------------------------------------------------
// main() Routine
// ----------------------------------------------------------------------------
int
main (void)
{

  volatile uint8_t rst;
//  uint8_t i, j;
//  char logbuf[16];
  char num_1[1] = "1";
  //Enter default mode
  rst = RSTSRC;

  enter_DefaultMode_from_RESET ();

  //LED0 = 0;
  PIN1 = 0;
  PIN2 = 0;
  PIN3 = 0;
  PIN4 = 0;
  PIN5 = 0;
  PIN6 = 0;
  //IE_EA = 1;
//  while (!SDA)
//    {
//      // Provide clock pulses to allow the slave to advance out
//      // of its current state. This will allow it to release SDA.
//      XBR2 = XBR2_XBARE__ENABLED;       // Enable Crossbar
//      SCL = 0;                          // Drive the clock low
//      for (i = 0; i < 255; i++)
//        ;        // Hold the clock low
//      SCL = 1;                          // Release the clock
//      while (!SCL)
//        ;                     // Wait for open-drain
//                              // clock output to rise
//      for (i = 0; i < 10; i++)
//        ;         // Hold the clock high
//      XBR2 = XBR2_XBARE__DISABLED;      // Disable Crossbar
//    }
  enter_Mode2_from_DefaultMode ();

  //sprintf (logbuf, "%d ", rst);
  //send_string_serial (logbuf);
  // send_string_serial ("Reseted\r\n");

//  CSLIB_commInit ();

// Configures all peripherals controlled by capsense, including
// the sensing block and port pins
  CSLIB_initHardware ();

  // Initializes the capsense variables and performs some scans to
  // initialize the baselines
  CSLIB_initLibrary ();
//  P0 |= 0x01;
//  P2MDOUT |= 0x80;
//  P2 &= ~0x80;

  //ready_for_new_touch = 0;
  // Initializes the UART interface

  //BSP_DISP_EN = 0;
//  BSP_LED0 = BSP_LED_OFF;

// Initialize the outgoing data array in case a read is done before a
// write
//  for (i = 0; i < NUM_BYTES_RD; i++)
//    {
//      SMB_DATA_OUT[i] = 0xFD;
//    }

//  SMB0_reset ();

  // Use Timer 1 for SCL clock rate
  // Enable SCL low timeout using Timer 3
//  SMB0_init (SMB0_TIMER0, true);

  // Set slave address for hardware ack
  // Assign RX buffer to store received data
  // (allocate an additional byte so we don't get an extra
  // SMB0_commandReceivedCb() call due to the buffer
  // being full)
  // SMB0_initSlave (SLAVE_ADDR, SMB_DATA_IN, NUM_BYTES_RD + 1);
//  send_string_serial ("Inited\r\n");
  while (1)
    {
      if (smb_rec)
        {
          //  send_string_serial ("I2C data: ");
          //send_data_serial (smb_data, NUM_BYTES_WR);
          //send_string_serial ("\r\n");
          smb_rec = 0;
        }
      //send_string_serial ("Running\r\n");
      CSLIB_update ();
      //send_string_serial ("update\r\n");
      //CSLIB_commUpdate ();
      //send_string_serial ("commupdate\r\n");
      // -----------------------------------------------------------------------------
      // If low power features are enabled, this will either put the device into a low
      // power state until it is time to take another scan, or put the device into a
      // low-power sleep mode if no touches are active
      // -----------------------------------------------------------------------------

//      CSLIB_lowPowerUpdate ();

      if (CSLIB_anySensorDebounceActive ())
        {
          LED0 = 0;
          Touch_status = 1;
        }
      else
        {
          LED0 = 1;
          Touch_status = 0;
        }

      if (Touch_status)
        {
          PIN1 = CSLIB_isSensorDebounceActive (0);
          PIN2 = CSLIB_isSensorDebounceActive (1);
          PIN3 = CSLIB_isSensorDebounceActive (2);
          PIN4 = CSLIB_isSensorDebounceActive (3);
          PIN5 = CSLIB_isSensorDebounceActive (4);
          PIN6 = CSLIB_isSensorDebounceActive (5);
        }
//      else
//        ready_for_new_touch = 1;

    }
}

//-----------------------------------------------------------------------------
// Callbacks
//-----------------------------------------------------------------------------

// Callback is called when the slave receives data from the master
void
SMB0_commandReceivedCb ()
{
//  uint8_t i;
//  uint8_t len = SMB0_getCommandLength ();
//
//  // Don't send more data than will fit in the TX buffer
//  if (len > NUM_BYTES_WR)
//    {
//      len = NUM_BYTES_WR;
//    }
//
//  if (SMB_DATA_IN[0] < NO_OF_CAP_CHANNELS)
//    SMB_DATA_OUT[0] = cap_channel_touched[SMB_DATA_IN[0]];
//  else
//    {
//          SMB_DATA_OUT[0] = SMB_DATA_IN[0];
//    }
//  for (i = 1; i < len; i++)
//    SMB_DATA_OUT[i] = SMB_DATA_IN[i];
////  if (SMB_DATA_IN[0] == 0xAA)
////    {
////      LED0 = 0;
////    }
////  if (SMB_DATA_IN[0] == 0xBB)
////    {
////      LED0 = 1;
////    }
//  smb_rec = 1;
//
//  // Send the response to the master
//  SMB0_sendResponse (SMB_DATA_OUT, len);
//
////  BSP_LED0 = !BSP_LED0;
}

// Callback is called when an SMBus error occurs
void
SMB0_errorCb (SMB0_TransferError_t error)
{
  switch (error)
    {
    case SMB0_ARBLOST_ERROR:
    case SMB0_NACK_ERROR:
    case SMB0_UNKNOWN_ERROR:
    case SMB0_TXUNDER_ERROR:
      numErrors++;
      break;
    } // Abort the transfer
  SMB0_abortTransfer ();

  // Transfer is complete (error)
  transferInProgress = false;
}

void
SMB0_transferCompleteCb ()
{
  transferInProgress = false;
}

void
UART0_receiveCompleteCb ()
{
//  uint8_t i;
//
//  for (i = 0; i < BUFFER_LENGTH; i++)
//    {
//      command_str[i] = buffer[i];
//    }
//  command_str[BUFFER_LENGTH] = 0;
//  //UART0_writeBuffer (buffer, BUFFER_LENGTH);
//  process_cmd (command_str);

//  if ((char) *buffer == '\n')
//    {
//      curr_ptr = 0;
//      return;
//    }
//  main_buffer[curr_ptr++] = *buffer;
//  if (curr_ptr == 8)
//    {
//      process_cmd (main_buffer);
//      curr_ptr = 0;
//    }
}

//void
//UART0_transmitCompleteCb ()
//{
//  Tx_ready = 1;
//}
